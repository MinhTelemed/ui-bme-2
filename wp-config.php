<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bme_ui' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z+x ?0UdN5bGb;%510ul*R#i&]Y{}j6I51tbByP8<1SyuSrI%_oVV<m/Z9hu`[Gf' );
define( 'SECURE_AUTH_KEY',  'G1>=[fgdlKa[KX34.^=RbuanK5*9/)N&C([EHS.Boo#&SYWm12X<sx&,4696&XE,' );
define( 'LOGGED_IN_KEY',    'IYj-v3Ua3Ps[XdJ~ OPreTf+I!&e<^T!&P?3a$&5T4}CC4 vjvRekw6|3w9_= `<' );
define( 'NONCE_KEY',        'WgL}y3.l.-NG#OANDZ<e30;hdOtk7{3aSSjQv(X.}T,$r5vNOMke|PS NDo)9Rgi' );
define( 'AUTH_SALT',        'U_%Z(AJEE+~Qg#Vl)$4Bah9okqMvQ%x2{!4<hO 0y?qRu,=i<L$1~D=]noN.F=&u' );
define( 'SECURE_AUTH_SALT', '0}AmUaR(g*Q6?vL%+^]R2t4Kde]XP{s%**-8@vRiE%$-Uh37Aic*@n2aIiP}>q,(' );
define( 'LOGGED_IN_SALT',   's.@  8+!Z=2UecSro-5;fJDh`Hv6vg/;4x$iv3:Z~S+qJoUU;><(y./&fLO,jNI?' );
define( 'NONCE_SALT',       '?FBTYF2YRmcK):ecti6E)#{nLo)7u9yr%:;nw$%Rug)L|I@U/4SGpiEZ[7*pjNqI' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
